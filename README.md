How to run:
python3 solution.py basket\ \(002\).csv 

see example below

[userone@test AAEProgExec]$ python3 solution.py basket\ \(002\).csv 

Total number of fruit(s): 22



Types of fruit(s): 5



The number of each type of fruit in descending order:

orange : 6
apple : 5
pineapple : 4
grapefruit : 4
watermelon : 3



The characteristics (size, color, shape, etc.) of each fruit by type:

3 apples : red , sweet

5 oranges : round , sweet

2 pineapples : prickly , sweet

1 apples : sweet
 , yellow
2 grapefruits : bitter
 , yellow
1 watermelons : green , heavy

1 apples : green , tart

2 grapefruits : bitter , yellow

2 watermelons : green
 , heavy
1 oranges : round
 , sweet
2 pineapples : prickly
 , sweet



Have any fruit been in the basket for over 3 days

2 pineapple
1 apple
1 watermelon
2 orange


