class fruit:
   
    counter = 0
    setType = set()
    allFruits = dict()
    #char1Set = set()
    #char2Set = set()
    basket = dict()

    delimiter = '\t'

    def __init__(self, name, days, characteristic1, characteristic2):
        self.name=name
        self.days=days
        #should probably use enumerated type but hey
        self.characteristic1=characteristic1
        self.characteristic2=characteristic2
        fruit.setType.add(name)
        #fruit.char1Set.add(characteristic1)
        #fruit.char2Set.add(characteristic2)
        fruit.counter += 1
        
        if( name in fruit.basket.keys() ):
            value = fruit.basket.get(name)
            fruit.basket.update( {name: value +1 } )
        else:
            fruit.basket.update( {name: 1 } )

        #thinking of both not being equal
        if(characteristic1 < characteristic2):
            tupleVal = (name, characteristic1, characteristic2)
        else:
            tupleVal = (name, characteristic2, characteristic1)

        if( tupleVal in fruit.allFruits.keys() ):
            value = fruit.allFruits.get(tupleVal)
            fruit.allFruits.update( {tupleVal: value +1 } )
        else:
            fruit.allFruits.update( {tupleVal: 1 } )

    def istAlt(self):
        return True if self.days > 3 else False
    def isAttrib1(self, attribute):
        return True if self.characteristic1 == attribute else False
    def isAttrib2(self, attribute):
        return True if self.characteristic2 == attribute else False
    
        
    def setName(self, val):
        self.name = val
    def setDays(self, val):
        self.days = val
    def setChar1(self, val):
        self.characteristic1 = val
    def setChar2(self, val):
        self.characteristic2 = val
   
    def __str__(self):
        delimiter = self.delimiter
        return "Name: " + self.name + " und hab existed fur tage: " + str(self.days) + delimiter + " and it is " + self.characteristic1 + " und "  + self.characteristic2  
            

    def getName(self):
        return self.name
    def getDays(self):
        return self.days
    def getChar1(self):
        return self.characteristic1
    def getChar2(self):
        return self.characteristic2
    def getType(self):
        return self.setType
    def getAllFruits(self):
        return self.allFruits
    '''
    def getChar1Set(self):
        return self.char1Set
    def getChar2Set(self):
        return self.char2Set
    '''
    def getCounter(self):
        return self.counter
    def getBasket(self):
        return self.basket
   