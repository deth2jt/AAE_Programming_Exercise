import csv
import sys
import fruit
import math


def get_fruit_list(fileName):
    fList = []

    with open(fileName) as fp:
        line = fp.readline()
        cnt = 1
        while line:
            if(cnt > 1):
                val = line.split(",")
                #print(val[0])
                f = fruit.fruit(val[0], int(val[1]), val[2].strip(" "), val[3].strip(" ").strip('\n') )
                fList += [f]
                #print(f.getType())
                #print(f.getCounter())
                #print(f.getBasket())
                #print(cnt)
                #print("val 3" + repr(val[3].strip(" ")) + "foo")

            line = fp.readline()
            cnt += 1
        
    return fList

if __name__ == '__main__':
    #from optparse import OptionParser
    #parser = OptionParser()
    #expecting basket not to be empty
    lst = get_fruit_list("basket (002).csv")
    print("List of fruits")
    for item in lst:
        print(item)
    print("\n\n")

    print("Total number of fruit(s):", lst[0].getCounter())
    print("\n\n")

    print("Types of fruit(s):", len(lst[0].getType()))
    print("\n\n")

    print("The number of each type of fruit in descending order:")
    sorted_values = sorted(lst[0].getBasket().items(),  key=lambda item: item[1], reverse=True) 
    #for key,value in sorted(lst[0].getBasket().items(), key=key):
    for key,value in sorted_values:
        print(key + " : " + str(value))
    print("\n\n")

    print("The characteristics (size, color, shape, etc.) of each fruit by type:")
    #sorted_values = sorted(lst[0].allFruits().items(),  key=lambda item: item[1], reverse=True) 
    #for key,value in sorted(lst[0].getBasket().items(), key=key):
    for key,value in lst[0].getAllFruits().items():
        print(str(value) + " " + key[0] + "(s) : " + key[1] + " , " + key[2])
    print("\n\n")

    print("Have any fruit been in the basket for over 3 days")
    result = filter(lambda item: item.istAlt(), lst) 
    altDict = dict()
    for oldfruit in list(result):
        #print(oldfruit) 
        if( oldfruit.getName() in altDict.keys() ):
            value = altDict.get(oldfruit.getName())
            altDict.update( {oldfruit.getName(): value +1 } )
        else:
            altDict.update( {oldfruit.getName(): 1 } )
    for key,value in altDict.items():
        print(str(value) + " " +  key + "(s)" )
    print("\n\n")
    



    

